document.getElementById("fortyMinuteMeetingTimerInnerLeft").addEventListener("click", buttonPressHideTime);
document.getElementById("fortyMinuteMeetingTimerInnerRight").addEventListener("click", buttonPressShowTime);

document.getElementById("tableTopicSpeech").addEventListener("click", buttonPressTableTopicSpeech);
document.getElementById("evaluationSpeech").addEventListener("click", buttonPressEvaluationSpeech);
document.getElementById("iceBreakerSpeech").addEventListener("click", buttonPressIceBreakerSpeech);
document.getElementById("projectSpeech").addEventListener("click", buttonPressProjectSpeech);
document.getElementById("manualControlSpeech").addEventListener("click", buttonPressManualControlSpeech);

document.getElementById("buttonStart").disabled=true;
document.getElementById("buttonStop").disabled=true;
document.getElementById("buttonClear").disabled=true;

function buttonPressHideTime(){
	document.getElementById("counterBoxMinutes").style.display="none";
	document.getElementById("counterBoxSeconds").style.display="none";
}

function buttonPressShowTime(){
	document.getElementById("counterBoxMinutes").style.display="block";
	document.getElementById("counterBoxSeconds").style.display="block";
}

function clearLights(){
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";
}

function buttonPressTableTopicSpeech(){
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Table Topic";

	document.getElementById("evaluationSpeech").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;
	document.getElementById("projectSpeech").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer1);
}

function buttonPressEvaluationSpeech(){
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Evaluation";

	document.getElementById("tableTopicSpeech").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;
	document.getElementById("projectSpeech").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer2);
};

function buttonPressIceBreakerSpeech(){
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Ice Breaker";

	document.getElementById("tableTopicSpeech").disabled=true;
	document.getElementById("evaluationSpeech").disabled=true;
	document.getElementById("projectSpeech").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer3);
};

function buttonPressProjectSpeech(){
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Project";

	document.getElementById("tableTopicSpeech").disabled=true;
	document.getElementById("evaluationSpeech").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer4);
};

function buttonPressManualControlSpeech(){
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Manual Control";
	document.getElementById("counterBoxMessages").innerHTML="Clear Lights"
	document.getElementById("counterBoxMessages").style.backgroundColor="lightgreen";

	document.getElementById("tableTopicSpeech").disabled=true;
	document.getElementById("evaluationSpeech").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;
	document.getElementById("projectSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer5);
};

document.getElementById("buttonClear").addEventListener("click", clearAll);

function clearAll(){
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("counterBoxMessages").innerHTML="Timer Status";
	document.getElementById("counterBoxMessages").style.backgroundColor="white";
	document.getElementById("counterBoxSeconds").innerHTML="0";
	document.getElementById("counterBoxMinutes").innerHTML="0";
	document.getElementById("selectedSpeechButtonValue").innerHTML="None Selected";
	document.getElementById("selectedSpeechButtonValue").style.backgroundColor="white";

	document.getElementById("tableTopicSpeech").disabled=false;
	document.getElementById("evaluationSpeech").disabled=false;
	document.getElementById("iceBreakerSpeech").disabled=false;
	document.getElementById("projectSpeech").disabled=false;
	document.getElementById("manualControlSpeech").disabled=false;

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").removeEventListener("click", startTimer1);
	document.getElementById("buttonStart").removeEventListener("click", startTimer2);
	document.getElementById("buttonStart").removeEventListener("click", startTimer3);
	document.getElementById("buttonStart").removeEventListener("click", startTimer4);
	document.getElementById("buttonStart").removeEventListener("click", startTimer5);

	/*document.getElementById("buttonStop").removeEventListener("click", stopNow);*/
};

/*----------------------------------------------------------------TABLE TOPIC-------------------------------------------------------*/

function startTimer1(){

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("tableTopicSpeech").disabled=true;

	let a;
	let b;
	let int1=setInterval(function(){addSeconds1()}, 1000);

	function stopNow(){
		clearInterval(int1);
		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;
		delete window.a;
		delete window.b;
	}

	document.getElementById("buttonStop").addEventListener("click", stopNow);			
	return;
};

function addSeconds1(){

	if(typeof a === "undefined"){
		
		a=1;
		b=1;
		
		document.getElementById("counterBoxMessages").innerHTML="Started";

		let c = Math.floor(b/60);
		document.getElementById("counterBoxMinutes").innerHTML=c;
		/*if(a%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML="0"+a;
			a=a+1;
			b=b+1;
		}
		else if(a/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML="0"+a;
			a=0;
			b=b+1;
		}
		else{*/
			document.getElementById("counterBoxSeconds").innerHTML="0"+a;
			a=a+1;
			b=b+1;
		/*};*/
	}
	else{
		if(b<60){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(b>=60 && b<90){
			document.getElementById("greenLight").style.backgroundColor='rgb('+0+','+150+','+0+')';
			document.getElementById("greenLightText").innerHTML="G R E E N";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(b>=90 && b<120){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor='rgb('+200+','+200+','+0+')';
			document.getElementById("yellowLightText").innerHTML="YELLOW";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(b>=120 && b<=150){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("redLightText").innerHTML="R E D";
		}
		else if(b>150){
			document.getElementById("greenLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("greenLight").style.border="4px solid red";
			document.getElementById("greenLightText").innerHTML="D Q";
			document.getElementById("yellowLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("yellowLight").style.border="4px solid red";
			document.getElementById("yellowLightText").innerHTML="D Q";
			document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("redLightText").innerHTML="D Q";
		};

		document.getElementById("counterBoxMessages").innerHTML="Started";

		let c = Math.floor(b/60);
		document.getElementById("counterBoxMinutes").innerHTML=c;

		/*if(a%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=a+1;
			b=b+1;
		}*/
		if(a<10){
			document.getElementById("counterBoxSeconds").innerHTML="0"+a;
			a=a+1;
			b=b+1;
		}
		else if(a/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=0;
			b=b+1;
		}
		else{
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=a+1;
			b=b+1;
		};
	};
};

/*-----------------------------------------------------------------EVALUATION-----------------------------------------------------*/

function startTimer2(){

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("evaluationSpeech").disabled=true;

	let d;
	let e;
	let int2=setInterval(function(){addSeconds2()}, 1000);

	function stopNow(){
		clearInterval(int2);
		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;
		delete window.d;
		delete window.e;
	}

	document.getElementById("buttonStop").addEventListener("click", stopNow);	
	return;
};

function addSeconds2(){

	if(typeof d === "undefined"){

		d=1;
		e=1;
		
		document.getElementById("counterBoxMessages").innerHTML="Started";

		let f = Math.floor(e/60);
		document.getElementById("counterBoxMinutes").innerHTML=f;

		/*if(d%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=d;
			d=d+1;
			e=e+1;
		}
		else if(d/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=d;
			d=0;
			e=e+1;
		}
		else{*/
			document.getElementById("counterBoxSeconds").innerHTML="0"+d;
			d=d+1;
			e=e+1;
		/*};*/
	}
	else{
		if(e<120){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(e>=120 && e<150){
			document.getElementById("greenLight").style.backgroundColor='rgb('+0+','+150+','+0+')';
			document.getElementById("greenLightText").innerHTML="G R E E N";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(e>=150 && e<180){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor='rgb('+200+','+200+','+0+')';
			document.getElementById("yellowLightText").innerHTML="YELLOW";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(e>=180 && e<=210){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("redLightText").innerHTML="R E D";
		}
		else if(e>210){
			document.getElementById("greenLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("greenLight").style.border="4px solid red";
			document.getElementById("greenLightText").innerHTML="D Q";
			document.getElementById("yellowLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("yellowLight").style.border="4px solid red";
			document.getElementById("yellowLightText").innerHTML="D Q";
			document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("redLightText").innerHTML="D Q";
		};

		document.getElementById("counterBoxMessages").innerHTML="Started";

		var f = Math.floor(e/60);
		document.getElementById("counterBoxMinutes").innerHTML=f;
		/*if(d%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=d;
			d=d+1;
			e=e+1;
		}*/
		if(d<10){
			document.getElementById("counterBoxSeconds").innerHTML="0"+d;
			d=d+1;
			e=e+1;
		}
		else if(d/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=d;
			d=0;
			e=e+1;
		}
		else{
			document.getElementById("counterBoxSeconds").innerHTML=d;
			d=d+1;
			e=e+1;
		};
	};
};

/*--------------------------------------------------------------------ICE BREAKER-------------------------------------------------*/

function startTimer3(){

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;

	let g;
	let h;
	let int3=setInterval(function(){addSeconds3()}, 1000);

	function stopNow(){
		clearInterval(int3);
		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;
		delete window.g;
		delete window.h;
	}

	document.getElementById("buttonStop").addEventListener("click", stopNow);	
	return;
};

function addSeconds3(){

	if(typeof g === "undefined"){
		
		g=1;
		h=1;
		
		document.getElementById("counterBoxMessages").innerHTML="Started";

		let i = Math.floor(h/60);
		document.getElementById("counterBoxMinutes").innerHTML=i;
		/*if(g%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=g;
			g=g+1;
			h=h+1;
		}
		else if(g/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=g;
			g=0;
			h=h+1;
		}
		else{*/
			document.getElementById("counterBoxSeconds").innerHTML="0"+g;
			g=g+1;
			h=h+1;
		/*};*/
	}
	else{
		if(h<240){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(h>=240 && h<300){
			document.getElementById("greenLight").style.backgroundColor='rgb('+0+','+150+','+0+')';
			document.getElementById("greenLightText").innerHTML="G R E E N";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(h>=300 && h<360){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor='rgb('+200+','+200+','+0+')';
			document.getElementById("yellowLightText").innerHTML="YELLOW";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(h>=360 && h<=390){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("redLightText").innerHTML="R E D";
		}
		else if(h>390){
			document.getElementById("greenLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("greenLight").style.border="4px solid red";
			document.getElementById("greenLightText").innerHTML="D Q";
			document.getElementById("yellowLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("yellowLight").style.border="4px solid red";
			document.getElementById("yellowLightText").innerHTML="D Q";
			document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("redLightText").innerHTML="D Q";
		};

		document.getElementById("counterBoxMessages").innerHTML="Started";

		let i = Math.floor(h/60);
		document.getElementById("counterBoxMinutes").innerHTML=i;

		/*if(g%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=g;
			g=g+1;
			h=h+1;
		}*/
		if(g<10){
			document.getElementById("counterBoxSeconds").innerHTML="0"+g
			g=g+1;
			h=h+1;
		}
		else if(g/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=g;
			g=0;
			h=h+1;
		}
		else{
			document.getElementById("counterBoxSeconds").innerHTML=g;
			g=g+1;
			h=h+1;
		};
	};
};


/*-----------------------------------------------------------------PROJECT---------------------------------------------------------*/

function startTimer4(){

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("projectSpeech").disabled=true;

	let j;
	let k;
	let int4=setInterval(function(){addSeconds4()}, 1000);

	function stopNow(){
		clearInterval(int4);
		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;
		delete window.j;
		delete window.k;
	}

	document.getElementById("buttonStop").addEventListener("click", stopNow);	
	return;
};

function addSeconds4(){

	if(typeof j === "undefined"){
		
		j=1;
		k=1;
		
		document.getElementById("counterBoxMessages").innerHTML="Started";

		let l = Math.floor(k/60);
		document.getElementById("counterBoxMinutes").innerHTML=l;
		/*if(j%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=j;
			j=j+1;
			k=k+1;
		}
		else if(j/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=j;
			j=0;
			k=k+1;
		}
		else{*/
			document.getElementById("counterBoxSeconds").innerHTML="0"+j;
			j=j+1;
			k=k+1;
		/*};*/
	}
	else{
		if(k<300){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(k>=300 && k<360){
			document.getElementById("greenLight").style.backgroundColor='rgb('+0+','+150+','+0+')';
			document.getElementById("greenLightText").innerHTML="G R E E N";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(k>=360 && k<420){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor='rgb('+200+','+200+','+0+')';
			document.getElementById("yellowLightText").innerHTML="YELLOW";
			document.getElementById("redLight").style.backgroundColor="black";
		}
		else if(k>=420 && k<=450){
			document.getElementById("greenLight").style.backgroundColor="black";
			document.getElementById("yellowLight").style.backgroundColor="black";
			document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("redLightText").innerHTML="R E D";
		}
		else if(k>450){
			document.getElementById("greenLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("greenLight").style.border="4px solid red";
			document.getElementById("greenLightText").innerHTML="D Q";
			document.getElementById("yellowLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("yellowLight").style.border="4px solid red";
			document.getElementById("yellowLightText").innerHTML="D Q";
			document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
			document.getElementById("redLightText").innerHTML="D Q";
		};

		document.getElementById("counterBoxMessages").innerHTML="Started";

		let l = Math.floor(k/60);
		document.getElementById("counterBoxMinutes").innerHTML=l;

		/*if(j%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=j;
			j=j+1;
			k=k+1;
		}*/
		if(j<10){
			document.getElementById("counterBoxSeconds").innerHTML="0"+j
			j=j+1;
			k=k+1;
		}
		else if(j/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=j;
			j=0;
			k=k+1;
		}
		else{
			document.getElementById("counterBoxSeconds").innerHTML=j;
			j=j+1;
			k=k+1;
		};
	};
};

/*--------------------------------------------------------------MANUAL CONTROL-----------------------------------------------------*/

function startTimer5(){

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	let m;
	let n;
	let int5=setInterval(function(){addSeconds5()}, 1000);

	function stopNow(){
		clearInterval(int5);
		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;

		document.getElementById("counterBoxMessages").removeEventListener("click", clearLights);
		document.getElementById("selectedSpeechButtonValue").removeEventListener("click", threeRedOvertime);
		document.getElementById("greenLight").removeEventListener("click", turnGreen);
		document.getElementById("yellowLight").removeEventListener("click", turnYellow);
		document.getElementById("redLight").removeEventListener("click", turnRed);

		delete window.m;
		delete window.n;
	}

	function threeRedOvertime(){
		document.getElementById("greenLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
		document.getElementById("greenLight").style.border="4px solid red";
		document.getElementById("yellowLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
		document.getElementById("yellowLight").style.border="4px solid red";
		document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
		document.getElementById("greenLightText").innerHTML="D. Q.";
        document.getElementById("yellowLightText").innerHTML="D. Q.";
        document.getElementById("redLightText").innerHTML="D. Q.";
	};

	document.getElementById("buttonStop").addEventListener("click", stopNow);	
	document.getElementById("selectedSpeechButtonValue").addEventListener("click", threeRedOvertime);
	return;
};

function turnGreen(){
	document.getElementById("greenLight").style.backgroundColor='rgb('+0+','+150+','+0+')';
	document.getElementById("greenLightText").innerHTML="G R E E N";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.backgroundColor="black";
};
function turnYellow(){
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.backgroundColor='rgb('+200+','+200+','+0+')';
	document.getElementById("yellowLightText").innerHTML="YELLOW";
	document.getElementById("redLight").style.backgroundColor="black";
};
function turnRed(){
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.backgroundColor='rgb('+255+','+80+','+80+')';
	document.getElementById("redLightText").innerHTML="R E D";
};

function addSeconds5(){

	if(typeof m === "undefined"){
		
		m=1;
		n=1;
		
		/*document.getElementById("counterBoxMessages").innerHTML="Started";*/
		document.getElementById("selectedSpeechButtonValue").innerHTML="3-Red Overtime";
		document.getElementById("selectedSpeechButtonValue").style.backgroundColor="pink";

		let o = Math.floor(n/60);
		document.getElementById("counterBoxMinutes").innerHTML=o;
		/*if(m%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=m;
			m=m+1;
			n=n+1;
		}
		else if(m/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=m;
			m=0;
			n=n+1;
		}
		else{*/
			document.getElementById("counterBoxSeconds").innerHTML="0"+m;
			m=m+1;
			n=n+1;
		/*};*/
	}
	else{

		document.getElementById("counterBoxMessages").addEventListener("click", clearLights);

		document.getElementById("greenLight").addEventListener("click", turnGreen);
		document.getElementById("yellowLight").addEventListener("click", turnYellow);
		document.getElementById("redLight").addEventListener("click", turnRed);

		let o = Math.floor(n/60);
		document.getElementById("counterBoxMinutes").innerHTML=o;
		/*if(m%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=m;
			m=m+1;
			n=n+1;
		}*/
		if(m<10){
			document.getElementById("counterBoxSeconds").innerHTML="0"+m;
			m=m+1;
			n=n+1;
		}
		else if(m/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=m;
			m=0;
			n=n+1;
		}
		else{
			document.getElementById("counterBoxSeconds").innerHTML=m;
			m=m+1;
			n=n+1;
		};
	};
};