/*this will be javascript to replace myScript2.js and include a 40 minute meeting timer at the top of the float right div*/
(function(){
  /*let aaa = 1;
  let bbb = 1;*/ 
  let zzz=0;
  /*let biggestInt=setInterval(function(){theWholePage();}, 1000);

  function theWholePage(){*/
  	let ccc = Math.floor(bbb/60);
  	let ddd = Math.floor(ccc/60);
  	if(ddd===0 && ccc<10 && aaa<10){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+"0"+ccc+":"+"0"+aaa;
  		aaa=aaa+1;
  		bbb=bbb+1;
  	}
  	else if(ddd===0 && ccc<10 && aaa>=10 && aaa<60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+"0"+ccc+":"+aaa;
  		aaa=aaa+1;
  		bbb=bbb+1;
  	}
  	else if(ddd===0 && ccc<10 && aaa===60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+"0"+ccc+":"+"00";
  		aaa=1;
  		bbb=bbb+1;
  	}
  	else if(ddd===0 && ccc>=10 && ccc<60 && aaa<10){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+ccc+":"+"0"+aaa;
  		aaa=aaa+1;
  		bbb=bbb+1;
  	}
  	else if(ddd===0 && ccc>=10 && ccc<60 && aaa>=10 && aaa<60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+ccc+":"+aaa;
  		aaa=aaa+1;
  		bbb=bbb+1;
  	}
  	else if(ddd===0 && ccc>=10 && ccc<60 && aaa===60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+ccc+":"+"00";
  		aaa=1;
  		bbb=bbb+1;
  	}
  	else if(ddd>0 && ccc===60 && aaa===60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+"00"+":"+"00";
  		aaa=1;
  		bbb=bbb+1;
  	}
  	else if(ddd>0 && (ccc-60*ddd)<10 && aaa<10){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+"0"+(ccc-60*ddd)+":"+"0"+aaa;
  		aaa=aaa+1;
  		bbb=bbb+1;
  	}
  	else if(ddd>0 && (ccc-60*ddd)<10 && aaa>=10 && aaa<60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+"0"+(ccc-60*ddd)+":"+aaa;
  		aaa=aaa+1;
  		bbb=bbb+1;
  	}
  	else if(ddd>0 && (ccc-60*ddd)<10 && aaa===60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+"0"+(ccc-60*ddd)+":"+"00";
  		aaa=1;
  		bbb=bbb+1;
  	}
  	else if(ddd>0 && (ccc-60*ddd)>=10 && (ccc-60*ddd)<60 && aaa<10){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+(ccc-60*ddd)+":"+"0"+aaa;
  		aaa=aaa+1;
  		bbb=bbb+1;
  	}
  	else if(ddd>0 && (ccc-60*ddd)>=10 && (ccc-60*ddd)<60 && aaa>=10 && aaa<60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+(ccc-60*ddd)+":"+aaa;
  		aaa=aaa+1;
  		bbb=bbb+1;
  	}
  	else if(ddd>0 && (ccc-60*ddd)>=10 && (ccc-60*ddd)<60 && aaa===60){
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Meeting "+ddd+":"+(ccc-60*ddd)+":"+"00";
  		aaa=1;
  		bbb=bbb+1;
  	}
  	else{
  		document.getElementById("fortyMinuteMeetingTimer").innerHTML="Timer Error";
  	};
  

document.getElementById("tableTopicSpeech").addEventListener("click", buttonPressTableTopicSpeech);
document.getElementById("evaluationSpeech").addEventListener("click", buttonPressEvaluationSpeech);
document.getElementById("iceBreakerSpeech").addEventListener("click", buttonPressIceBreakerSpeech);
document.getElementById("projectSpeech").addEventListener("click", buttonPressProjectSpeech);
document.getElementById("manualControlSpeech").addEventListener("click", buttonPressManualControlSpeech);

if(zzz===0){
	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;
}

function clearLights(){
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";
}

function buttonPressTableTopicSpeech(){
	zzz=1;
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Table Topic";

	document.getElementById("evaluationSpeech").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;
	document.getElementById("projectSpeech").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer1);
}

function buttonPressEvaluationSpeech(){
	zzz=2;
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Evaluation";

	document.getElementById("tableTopicSpeech").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;
	document.getElementById("projectSpeech").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer2);
};

function buttonPressIceBreakerSpeech(){
	zzz=3;
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Ice Breaker";

	document.getElementById("tableTopicSpeech").disabled=true;
	document.getElementById("evaluationSpeech").disabled=true;
	document.getElementById("projectSpeech").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer3);
};

function buttonPressProjectSpeech(){
	zzz=4;
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Project";

	document.getElementById("tableTopicSpeech").disabled=true;
	document.getElementById("evaluationSpeech").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer4);
};

function buttonPressManualControlSpeech(){
	zzz=5;
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("selectedSpeechButtonValue").innerHTML="Manual Control";
	document.getElementById("counterBoxMessages").innerHTML="Clear Lights"
	document.getElementById("counterBoxMessages").style.backgroundColor="lightgreen";

	document.getElementById("tableTopicSpeech").disabled=true;
	document.getElementById("evaluationSpeech").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;
	document.getElementById("projectSpeech").disabled=true;

	document.getElementById("buttonStart").disabled=false;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").addEventListener("click", startTimer5);
};

document.getElementById("buttonClear").addEventListener("click", clearAll);

function clearAll(){
	zzz=0;
	document.getElementById("greenLight").style.border="4px solid green";
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.border="4px solid yellow";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.border="4px solid red";
	document.getElementById("redLight").style.backgroundColor="black";

	document.getElementById("counterBoxMessages").innerHTML="Timer Status";
	document.getElementById("counterBoxMessages").style.backgroundColor="white";
	document.getElementById("counterBoxSeconds").innerHTML="0";
	document.getElementById("counterBoxMinutes").innerHTML="0";
	document.getElementById("selectedSpeechButtonValue").innerHTML="None Selected";
	document.getElementById("selectedSpeechButtonValue").style.backgroundColor="white";

	document.getElementById("tableTopicSpeech").disabled=false;
	document.getElementById("evaluationSpeech").disabled=false;
	document.getElementById("iceBreakerSpeech").disabled=false;
	document.getElementById("projectSpeech").disabled=false;
	document.getElementById("manualControlSpeech").disabled=false;

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("buttonStart").removeEventListener("click", startTimer1);
	document.getElementById("buttonStart").removeEventListener("click", startTimer2);
	document.getElementById("buttonStart").removeEventListener("click", startTimer3);
	document.getElementById("buttonStart").removeEventListener("click", startTimer4);
	document.getElementById("buttonStart").removeEventListener("click", startTimer5);
};

/*----------------------------------------------------------------TABLE TOPIC-------------------------------------------------------*/

function startTimer1(){

	let a;
	let b;

	document.getElementById("counterBoxMessages").innerHTML="Started";

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("tableTopicSpeech").disabled=true;

	function stopNow(){
		clearInterval(int1);
		delete window.a;
		delete window.b;
		delete window.c;

		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;
	};

	document.getElementById("buttonStop").addEventListener("click", stopNow);

	let int1=setInterval(function(){ignition1()}, 1000);
	return;
};
function ignition1(){
	if (typeof a != "number"){
		a=1;
		b=1;
		let c=Math.floor(b/60);
		document.getElementById("counterBoxSeconds").innerHTML="0";
		document.getElementById("counterBoxMinutes").innerHTML="0";
	}
	else {
		let c=Math.floor(b/60);
		if (a<10){
			document.getElementById("counterBoxSeconds").innerHTML="0"+a;
			document.getElementById("counterBoxMinutes").innerHTML=c;
			a=a+1;
			b=b+1;
		}
		else if(a/60===1){
			document.getElementById("counterBoxSeconds").innerHTML="00";
			document.getElementById("counterBoxMinutes").innerHTML=c;
			a=1;
			b=b+1;
		}
		else{
			document.getElementById("counterBoxSeconds").innerHTML=a;
			document.getElementById("counterBoxMinutes").innerHTML=c;
			a=a+1;
			b=b+1;
		};
	};
};
	

/*-----------------------------------------------------------------EVALUATION-----------------------------------------------------*/

function startTimer2(){

	let startTimer2Int = setInterval(middleman(), 500);

	function middleMan(){
		let a;
		addNow();
	};

	function addNow(){
		if (typeof a === "undefined"){
			a=0;
			document.getElementById("counterBoxSeconds").innerHTML=a;
		}
		else{
			a=a+1;
			document.getElementById("counterBoxSeconds").innerHTML=a;
		};
		console.log("a = "+a);
	}

	document.getElementById("counterBoxMessages").innerHTML="Started";

	function stopNow(){
		clearInterval(startTimer2Int);
		delete window.a;
		delete window.b;
		delete window.c;
		delete document.a;
		delete document.b;
		delete document.c;

		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;
	};

	document.getElementById("buttonStop").addEventListener("click", function(){stopNow()});

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("evaluationSpeech").disabled=true;

	
};
/*--------------------------------------------------------------------ICE BREAKER-------------------------------------------------*/

function startTimer3(){

	document.getElementById("counterBoxMessages").innerHTML="Started";

	function stopNow(){
		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;
	};

	document.getElementById("buttonStop").addEventListener("click", stopNow);

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("iceBreakerSpeech").disabled=true;

	

};
/*-----------------------------------------------------------------PROJECT---------------------------------------------------------*/

function startTimer4(){

	document.getElementById("counterBoxMessages").innerHTML="Started";

	function stopNow(){
		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;
	};

	document.getElementById("buttonStop").addEventListener("click", stopNow);

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("projectSpeech").disabled=true;

	
};
/*--------------------------------------------------------------MANUAL CONTROL-----------------------------------------------------*/

function startTimer5(){

	document.getElementById("counterBoxMessages").innerHTML="Started/Clear";
	document.getElementById("selectedSpeechButtonValue").innerHTML="3-Red Overtime";
	document.getElementById("selectedSpeechButtonValue").style.backgroundColor="pink";
	document.getElementById("counterBoxMessages").addEventListener("click", clearLights);
	document.getElementById("selectedSpeechButtonValue").addEventListener("click", threeRedOvertime);
	document.getElementById("greenLight").addEventListener("click", turnGreen);
	document.getElementById("yellowLight").addEventListener("click", turnYellow);
	document.getElementById("redLight").addEventListener("click", turnRed);

	function turnGreen(){
		document.getElementById("greenLight").style.backgroundColor="green";
		document.getElementById("yellowLight").style.backgroundColor="black";
		document.getElementById("redLight").style.backgroundColor="black";
	};
	function turnYellow(){
		document.getElementById("greenLight").style.backgroundColor="black";
		document.getElementById("yellowLight").style.backgroundColor="yellow";
		document.getElementById("redLight").style.backgroundColor="black";
	};
	function turnRed(){
		document.getElementById("greenLight").style.backgroundColor="black";
		document.getElementById("yellowLight").style.backgroundColor="black";
		document.getElementById("redLight").style.backgroundColor="red";
	};

	function threeRedOvertime(){
		document.getElementById("greenLight").style.backgroundColor="red";
		document.getElementById("greenLight").style.border="4px solid red";
		document.getElementById("yellowLight").style.backgroundColor="red";
		document.getElementById("yellowLight").style.border="4px solid red";
		document.getElementById("redLight").style.backgroundColor="red";
	};

	function stopNow(){
		document.getElementById("counterBoxMessages").innerHTML="Stopped";
		document.getElementById("buttonStop").disabled=true;
		document.getElementById("buttonClear").disabled=false;

		document.getElementById("counterBoxMessages").removeEventListener("click", clearLights);
		document.getElementById("selectedSpeechButtonValue").removeEventListener("click", threeRedOvertime);
		document.getElementById("greenLight").removeEventListener("click", turnGreen);
		document.getElementById("yellowLight").removeEventListener("click", turnYellow);
		document.getElementById("redLight").removeEventListener("click", turnRed);
	};

	document.getElementById("buttonStop").addEventListener("click", stopNow);

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;
	document.getElementById("buttonClear").disabled=true;
	document.getElementById("manualControlSpeech").disabled=true;
};
})();