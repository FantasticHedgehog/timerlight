/*-------------------------------------------------------------------------------------------------------------------*/

document.getElementById("buttonStart").disabled=true;
document.getElementById("buttonStop").disabled=true;
document.getElementById("buttonClear").disabled=true;
/*document.getElementById("projectSpeech").disabled=true;*/
document.getElementById("iceBreakerSpeech").disabled=true;
document.getElementById("evaluationSpeech").disabled=true;
document.getElementById("tableTopicSpeech").disabled=true;
/*document.getElementById("manualControl").disabled=true;*/

/*--------------------------------------------------------------------------------------------------------------------*/

document.getElementById("buttonClear").addEventListener("click", clearAll)

/*--------------------------------------------------------------------------------------------------------------------*/

document.getElementById("tableTopicSpeech").addEventListener("click", tableTopic);
document.getElementById("evaluationSpeech").addEventListener("click", evaluation);
document.getElementById("iceBreakerSpeech").addEventListener("click", iceBreaker);
document.getElementById("projectSpeech").addEventListener("click", project);
document.getElementById("manualControlSpeech").addEventListener("click", manualControl);

/*--------------------------------------------------------------------------------------------------------------------*/

function clearAll(){

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=true;
	document.getElementById("buttonClear").disabled=true;

	document.getElementById("counterBoxMessages").innerHTML="Cleared";
	document.getElementById("selectedSpeechButtonValue").innerHTML="None Selected";
	document.getElementById("counterBoxSeconds").innerHTML="- -";
	document.getElementById("counterBoxMinutes").innerHTML="- -";

	document.getElementById("greenLight").removeEventListener("click", turnGreen);
	document.getElementById("yellowLight").removeEventListener("click", turnYellow);
	document.getElementById("redLight").removeEventListener("click", turnRed);

	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.backgroundColor="black";
};

/*-----------------------------------------------------------------------------------------------------------------*/

function turnGreen(){
	document.getElementById("greenLight").style.backgroundColor="green";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.backgroundColor="black";
}
function turnYellow(){
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.backgroundColor="yellow";
	document.getElementById("redLight").style.backgroundColor="black";
}
function turnRed(){
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.backgroundColor="red";
}
function blackOut(){
	document.getElementById("greenLight").style.backgroundColor="black";
	document.getElementById("yellowLight").style.backgroundColor="black";
	document.getElementById("redLight").style.backgroundColor="black";
}



/*--------------------------------------------------------------------------------------------------------------------*/

function tableTopic(){
	document.getElementById("selectedSpeechButtonValue").style.fontSize = "160%";
	document.getElementById("selectedSpeechButtonValue").innerHTML="Table Topic";
};

/*--------------------------------------------------------------------------------------------------------------------*/

function evaluation(){
	document.getElementById("selectedSpeechButtonValue").style.fontSize = "160%";
	document.getElementById("selectedSpeechButtonValue").innerHTML="Evaluation";
};

/*--------------------------------------------------------------------------------------------------------------------*/

function iceBreaker(){
	document.getElementById("selectedSpeechButtonValue").style.fontSize = "160%";
	document.getElementById("selectedSpeechButtonValue").innerHTML="Ice Breaker";
};

/*--------------------------------------------------------------------------------------------------------------------*/

function project(){

	document.getElementById("selectedSpeechButtonValue").style.fontSize = "160%";
	document.getElementById("selectedSpeechButtonValue").innerHTML="Project";
	document.getElementById("buttonStart").disabled=false;

	document.getElementById("buttonStart").addEventListener("click", startProject);
};
/*-------------------------------------------------------------------------------------------------------------------*/
function manualControl(){
	
	document.getElementById("selectedSpeechButtonValue").style.fontSize = "140%";
	document.getElementById("selectedSpeechButtonValue").innerHTML="Manual Control";
	document.getElementById("buttonStart").disabled=false;

	document.getElementById("buttonStart").addEventListener("click", startManualControl);
};

/*-------------------------------------------------------------------------------------------------------------------*/

function startProject(){

	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;

	let a=0;
	let b=0;
	let int = setInterval(function(){

	if(b<7){
		document.getElementById("greenLight").style.backgroundColor="black";
		document.getElementById("yellowLight").style.backgroundColor="black";
		document.getElementById("redLight").style.backgroundColor="black";
	}
	else if(b>=7 && b<9){
		document.getElementById("greenLight").style.backgroundColor="green";
		document.getElementById("yellowLight").style.backgroundColor="black";
		document.getElementById("redLight").style.backgroundColor="black";
	}
	else if(b>=9 && b<11){
		document.getElementById("greenLight").style.backgroundColor="black";
		document.getElementById("yellowLight").style.backgroundColor="yellow";
		document.getElementById("redLight").style.backgroundColor="black";
	}
	else if(b>=11 && b<=13){
		document.getElementById("greenLight").style.backgroundColor="black";
		document.getElementById("yellowLight").style.backgroundColor="black";
		document.getElementById("redLight").style.backgroundColor="red";
	}
	else{
		document.getElementById("greenLight").style.backgroundColor="red";
		document.getElementById("yellowLight").style.backgroundColor="red";
		document.getElementById("redLight").style.backgroundColor="red";
	}

		var c = Math.floor(b/60);	
		document.getElementById("counterBoxMinutes").innerHTML=c;
		document.getElementById("buttonStop").addEventListener("click", function(){
			clearInterval(int);
			document.getElementById("counterBoxMessages").innerHTML="Stopped";
			document.getElementById("buttonStop").disabled=true;
			document.getElementById("buttonClear").disabled=false;
			a=0;
			b=0;
			c=0;
		});
		document.getElementById("counterBoxMessages").innerHTML="Started";
		if(a%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=a+1;
			b=b+1;
		}
		else if(a/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=0;
			b=b+1;
		}
		else{
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=a+1;
			b=b+1;
		}
	},1000);
};

/*-----------------------------------------------------------------------------------------------------------------*/

function startManualControl(){
	
	document.getElementById("buttonStart").disabled=true;
	document.getElementById("buttonStop").disabled=false;

	document.getElementById("greenLight").addEventListener("click", turnGreen);
	document.getElementById("yellowLight").addEventListener("click", turnYellow);
	document.getElementById("redLight").addEventListener("click", turnRed);

	document.getElementById("counterBoxMessages").addEventListener("click", blackOut);

	let a=0;
	let b=0;
	let int = setInterval(function(){
		var c = Math.floor(b/60);	
		document.getElementById("counterBoxMinutes").innerHTML=c;
		document.getElementById("buttonStop").addEventListener("click", function(){
			clearInterval(int);
			document.getElementById("counterBoxMessages").innerHTML="Stopped";
			document.getElementById("buttonStop").disabled=true;
			document.getElementById("buttonClear").disabled=false;
			a=0;
			b=0;
			c=0;
		});
		document.getElementById("counterBoxMessages").innerHTML="Started";
		if(a%59 != 0){
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=a+1;
			b=b+1;
		}
		else if(a/59 == 1){
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=0;
			b=b+1;
		}
		else{
			document.getElementById("counterBoxSeconds").innerHTML=a;
			a=a+1;
			b=b+1;
		}
	},1000);
};

/*------------------------------------------------------------------------------------------------------------------*/
	